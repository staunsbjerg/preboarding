package com.findwise.preboarding;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class RecommenderControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void testModelA() {
        var response =
                restTemplate.getForEntity(
                        "/recommend/a", RecommenderResponse.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody())
                .extracting(RecommenderResponse::results)
                .asList()
                .containsExactly("Alice", "Bob", "Charlie", "Isaac", "Walter");
    }

    @Disabled
    @Test
    void testInterleaved() {
        var response =
                restTemplate.getForEntity(
                        "/recommend/c", RecommenderResponse.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody())
                .extracting(RecommenderResponse::results)
                .asList()
                .containsExactly(
                        "Alice", "Isaac", "Bob", "Victor", "Charlie", "Carol",
                        "Walter", "Justin");
    }
}

package com.findwise.preboarding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PreboardingApplication {

    public static void main(String[] args) {
        SpringApplication.run(PreboardingApplication.class, args);
    }
}

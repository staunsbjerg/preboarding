package com.findwise.preboarding;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecommenderResponse {

    private final List<String> results = new ArrayList<>();

    @JsonProperty(access = Access.READ_ONLY)
    public int numFound() {
        return results.size();
    }

    @JsonProperty
    public List<String> results() {
        return Collections.unmodifiableList(results);
    }

    @JsonIgnore
    public void addResult(final String result) {
        results.add(result);
    }
}

package com.findwise.preboarding;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/recommend")
public class RecommenderController {

    private final RecommenderService modelService;

    public RecommenderController(final RecommenderService modelService) {
        this.modelService = modelService;
    }

    @GetMapping(value = "/a", produces = MediaType.APPLICATION_JSON_VALUE)
    public RecommenderResponse modelA() {
        var response = new RecommenderResponse();
        modelService.modelA().forEach(response::addResult);
        return response;
    }

    @GetMapping(value = "/b", produces = MediaType.APPLICATION_JSON_VALUE)
    public RecommenderResponse modelB() {
        var response = new RecommenderResponse();
        modelService.modelB().forEach(response::addResult);
        return response;
    }

    @GetMapping(value = "/c", produces = MediaType.APPLICATION_JSON_VALUE)
    public RecommenderResponse interleaved() {
        var response = new RecommenderResponse();
        modelService.interleavedModelAB().forEach(response::addResult);
        return response;
    }
}

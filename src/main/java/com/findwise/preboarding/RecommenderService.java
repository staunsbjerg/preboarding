package com.findwise.preboarding;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class RecommenderService {

    private final Coin coin;

    public RecommenderService(final Coin coin) {
        this.coin = coin;
    }

    public List<String> modelA() {
        return List.of("Alice", "Bob", "Charlie", "Isaac", "Walter");
    }

    public List<String> modelB() {
        return List.of("Isaac", "Victor", "Carol", "Alice", "Justin");
    }

    public List<String> interleavedModelAB() {
        var modelA = modelA();
        var modelB = modelB();
        return coin.toss()
                ? teamDraft(modelA.iterator(), modelB.iterator())
                : teamDraft(modelB.iterator(), modelA.iterator());
    }

    private <T> List<T> teamDraft(
            final Iterator<T> first, final Iterator<T> second) {
        var combined = new ArrayList<T>();

        T next;
        while (first.hasNext() || second.hasNext()) {

            next = nextDraft(first);
            if (next != null) {
                combined.add(next);
            }

            next = nextDraft(second);
            if (next != null) {
                combined.add(next);
            }
        }

        return combined;
    }

    private <T> T nextDraft(final Iterator<T> it) {
        for (; ; ) {
            if (!it.hasNext()) {
                return null;
            }
            T value = it.next();

            // TODO: ensure that we dont return a value that we have already
            //  encountered
            if (isUnique(value)) {
                return value;
            }
        }
    }

    private <T> boolean isUnique(final T value) {
        // dedupe handling here.
        // we may need to change some method signatures
        // in order to support it.
        return true;
    }
}

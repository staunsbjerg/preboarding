package com.findwise.preboarding;

import java.util.Random;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Coin {
    private final Random random;

    public Coin(@Value("${coin.seed:0}") final long seed) {
        this.random = new Random(seed);
    }

    public boolean toss() {
        return random.nextBoolean();
    }
}

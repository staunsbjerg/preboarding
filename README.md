# Applicant assignment

## Background
I have a recommender system setup. 
You can see the results of it by querying
[http://localhost:8080/recommend/a](http://localhost:8080/recommend/a), but I
now also have a second recommender at
[http://localhost:8080/recommend/b](http://localhost:8080/recommend/b)

Our customer has tasked us to figure out which one is better. So I have added a
third recommender
[http://localhost:8080/recommend/c](http://localhost:8080/recommend/c) that
interleaves recommender a and b.

The interleaving is supposed to be implemented as team drafting like in the
school yard when kids are picking teams for a football match 

However, I forgot to handle de-duplication. So now there exists duplicates in 
the response

As you can see there exists 2 test cases for the RecommenderController.
There is one missing and one failing. The failing test is currently disabled.

## The assignment
I would like you to solve and document how you would solve the following task

1. Create a new branch from the HEAD of master.
2. Add a test for the missing endpoint (recommender b)
3. The assertion in the failing test is correct.
   However, the code in the service is not.
   I would like you to fix the code so that duplicates are removed
   and then enable the test
4. Consider how you would test when
   recommender b is selected first by the coin toss 
   (implementation is not necessary)

You may utilize the full internet - but try to limit your time
to approximately 1 hour before you settle upon a solution.

The important is how you will solve the task - not the result.
You can add your reflections in a text file and commit it too.

## Technologies

If you are familiar with

* Apache Maven
* GIT
* Spring Boot

It will make your task a lot easier.

## Hand-in

When done create a pull-request and add me as reviewer
